# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è rinominare i file di output prodotti dalla simulazione
#
# Call (supponendo di essere già nella cartella di build):
# $ python3 ../renamer.py
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import os
import sys
import shutil

# ==========================================================================================
#                                     INPUTS/PARAMETRI
# ==========================================================================================
# Definisco i parametri fondamentali per l'esecuzione dello script
ind_length = int(sys.argv[1])         # Numero di step del ciclo sulle lunghezze
ind_energy = int(sys.argv[2])       # Numero di step del ciclo sulle energie

# Definisco i path della simulazione
thisfolder = os.path.dirname(os.path.abspath(__file__))
outfolder = thisfolder + "/Outputfiles/"
outdefault = outfolder + "lossdata.dat"

# ==========================================================================================
#                               RINOMINAZIONE DEL FILE
# ==========================================================================================
# A simulazione terminata, rinomino il file appena creato e lo copio nella corretta destinazione
shutil.copy(thisfolder + '/CERN2022-build/lossdata.dat',thisfolder + '/Outputfiles/')
newoutname = outfolder + f'lossdata_length_{ind_length:03d}_energy_{ind_energy:03d}.dat'
os.rename(outdefault,newoutname)

# Butto il vecchio file
os.remove(thisfolder + '/CERN2022-build/lossdata.dat')

# Chiudo lo script
print(f"Python task completed! \n")