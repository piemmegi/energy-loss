# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è modificare il codice della simulazione di deposito energetico 
# "Energy-loss" (fascio gamma su materiale amorfo) imponendo valori predefiniti di energia
# iniziale e lunghezza del cristallo. Lo script non deve fare altro, poiché sarà il
# mastercall.sh a occuparsi del sourcing e del lancio della simulazione.
#
# Call (supponendo di essere già nella cartella di build):
# $ python3 ../mastercall.py
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import os
import sys

# ==========================================================================================
#                                         INPUTS
# ==========================================================================================
# Definisco i parametri fondamentali per l'esecuzione dello script
energy = sys.argv[1]                  # Energia da testare, in GeV
cryslength = sys.argv[2]              # Lunghezza da testare, in X0
crystal = sys.argv[3]                 # 'PbF2' oppure 'PbWO4
statistics = sys.argv[4]              # Statistica per ogni run (== per ogni energia e lunghezza)
ifDC = (sys.argv[5] == 'True')        # Se modificare anche il DetectorConstruction.cc o solo il run1.mac

# ==========================================================================================
#                                   PARAMETRI PRELIMINARI
# ==========================================================================================
# Definisco dove si trovi il DetectorConstruct e a quali righe dovrò modificare il codice
thisfolder = os.path.dirname(os.path.abspath(__file__))
dc_file = thisfolder + "/CERN2022/src/DetectorConstruction.cc"
line_ifPbF2 = 120
line_thick_scale = 121

# Definisco dove si troverà il file di input della simulazione e a quali righe dovrò modificare
# il codice
build_run1mac_file = thisfolder + "/CERN2022-build/run1.mac"
line_energy = 22
line_statistics = 23

# Definisco dove dovrò mettere i file di output
outfolder = thisfolder + "/Outputfiles/"
outdefault = outfolder + "lossdata.dat"

# ==========================================================================================
#                              EDIT DEL CODICE DI SIMULAZIONE
# ==========================================================================================
# Se necessario, apro il DetectorConstruction.cc e modifico il tipo di cristallo e la sua
# lunghezza.
if ifDC:
    with open(dc_file,'r') as dc:
        # Leggo tutte le righe del file
        print(f"Editing DetectorConstruction.cc file")
        lines = dc.readlines()

        # Modifico la riga dove definisco il materiale del cristallo
        if (crystal == 'PbF2'):
            lines[line_ifPbF2] = '\tG4bool ifPbF2 = true;\n'
        else:
            lines[line_ifPbF2] = '\tG4bool ifPbF2 = false;\n'

        # Modifico la riga dove definisco lo spessore del cristallo
        lines[line_thick_scale] = f'\tG4double thick_scale = {cryslength};\n'

    with open(dc_file,'w') as dc:
        # Scrivo le righe
        dc.writelines(lines)

# Se non ho toccato il DetectorConstruction.cc, devo modificare run1.mac per definire la 
# statistica della run e l'energia del fascio primario
if not ifDC:
    with open(build_run1mac_file,'r') as f:
        # Leggo tutte le righe del file
        print(f"Editing run1.mac file")
        lines = f.readlines()

        # Modifico la riga dove definisco la statistica
        lines[line_statistics] = f"/run/beamOn {statistics}\n"

        # Modifico la riga dove definisco l'energia del fascio
        lines[line_energy] = f'/gps/ene/mono {energy} GeV\n'

    with open(build_run1mac_file,'w') as f:
        # Scrivo le righe
        f.writelines(lines)

# Chiudo lo script
print(f"Python task completed! \n")