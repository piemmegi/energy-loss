#! /usr/bin/bash

# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è lanciare iterativamente la simulazione "Energy-loss" per un
# numero arbitrario di lunghezze di cristallo, per un tipo di materiale fissato, al variare
# dell'energia del fascio primario.
#
# Sintassi per chiamare lo script: (supponendo di essere già nella cartella di build!):
# $ ./mastercall.sh
#
# ==========================================================================================
#                                      PARAMETRI GLOBALI
# ==========================================================================================
# Definisco i parametri della run: energie da testare, tipo di cristallo, statistica per
# singola run, lunghezze da testare
energies=($(seq 0 2.5 120))         # Energia da testare, in GeV
cryslengths=($(seq 2 0.01 3.5))     # Lunghezza da testare, in X0
crystal='PbF2'                      # 'PbF2' oppure 'PbWO4'
statistics=10000                    # Statistica per ogni run (== per ogni energia e lunghezza)
ifVM='True'                        # Se sto lavorando in locale su WSL (False) o su VM (True)

# ==========================================================================================
#                                         SOURCING
# ==========================================================================================
# In base a se lavoro in VM o in locale, eseguo il sourcing di diversi programmi
# (Geant4, Compilatore C++, CMake)
if [ $ifVM == 'True' ];
then
    source /cvmfs/sft.cern.ch/lcg/contrib/gcc/9/x86_64-centos8/setup.sh
    source /cvmfs/geant4.cern.ch/geant4/11.0.p02/x86_64-centos8-gcc9-optdeb-MT/GNUMake-setup.sh
    source /cvmfs/sft.cern.ch/lcg/contrib/CMake/3.18.3/Linux-x86_64/setup.sh
else
    source ~/Geant4.11.0.2/geant4-11.0.2-install/bin/geant4.sh
fi

# Definisco il numero di thread per multithreading
export G4FORCENUMBEROFTHREADS=4

# ==========================================================================================
#                                        CONCEPT
# ==========================================================================================
# Itero su tutte le lunghezze di cristallo e su tutte le energie. A ogni step di lunghezza
# chiamo uno script Python che modifica il DetectorConstruction.cc, dopodiché compilo la 
# simulazione. Poi richiamo lo script Python e modifico il run1.mac per definire la statistica
# e l'energia del fascio primario e a quel punto posso chiamare la simulazione. A fine run
# devo spostare il file prodotto e rinominarlo, prima di passare al punto successivo.
#
# ==========================================================================================
#                                        SIMULAZIONE
# ==========================================================================================
# Itero su tutte le lunghezze di cristallo
nvalues_energies=${#energies[@]}
nvalues_lengths=${#cryslengths[@]}

for ((i = 0 ; i < nvalues_lengths ; i++)); do
	# Ad ogni step di lunghezza, modifico il DetectorConstruction.cc
	python3 ../mastercall_texteditor.py "${energies[0]}" "${cryslengths[i]}" "${crystal}" "${statistics}" "True"

	# Rimuovo il contenuto della cartella CERN2022-build
	python3 ../build_cleaner.py

	# Make della simulazione
    if [ $ifVM == 'True' ];
	then
        # In VM
        cmake "-DGeant4_DIR=/cvmfs/geant4.cern.ch/geant4/11.0.p02/x86_64-centos8-gcc9-optdeb-MT/lib64/Geant4-11.0.2/" "../CERN2022"
        make "-j8"
    else
        # In locale
        cmake "-DGeant4_DIR=~/Geant4.11.0.2/geant4-11.0.2-install/lib/Geant4-11.0.2/" "../CERN2022"
        make "-j4"
    fi

    # Itero su tutte le lunghezze dei cristalli
    for ((j = 0 ; j < nvalues_energies ; j++)); do
    	# Modifico il run1.mac
   	python3 ../mastercall_texteditor.py "${energies[j]}" "${cryslengths[i]}" "${crystal}" "${statistics}" "False"

   	# Lancio la simulazione
   	./exampleB1 run1.mac

        # A simulazione terminata, rinomino il file appena creato e lo copio nella corretta destinazione
        # usando un altro script Python
        python3 ../mastercall_renamer.py "$i" "$j"
    done
done

# Finito!
echo "Done!"
