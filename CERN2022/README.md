# A short guide on how to edit this simulation

Suppose that you want to perform small edits to this simulation, such as:
- Define a different primary beam (energy and/or type)
- Define a different statistics for the runs
- Define a different crystal target (length and/or type)

Then you should modify the following files:

- `/run1.mac`
	Here you can set the the beam particle type, the beam energy and the number of events (lines 23-24)

- `/src/DetectorConstruction.cc`
	Here you can set the properties of the crystal target (lines 121-122)