//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file DetectorConstruction.cc
/// \brief Implementation of the B1::DetectorConstruction class

#include "DetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Orb.hh"
#include "G4Sphere.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4SystemOfUnits.hh"
#include "G4RegionStore.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4UniformMagField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"


// Color
#include "G4VisAttributes.hh"
#include "G4Colour.hh"

DetectorConstruction::DetectorConstruction()
{}

DetectorConstruction::~DetectorConstruction()
{}

G4VPhysicalVolume* DetectorConstruction::Construct()
{
	//===================================================================================================
    //                                       DEFINITION OF THE MATERIALS
    //===================================================================================================
    // Get NIST material manager
	// (see https://geant4-userdoc.web.cern.ch/UsersGuides/ForApplicationDeveloper/html/Appendix/materialNames.html)
    G4NistManager* nist = G4NistManager::Instance();

    // Get from NIST the PbWO4 material
    G4Material* PbWO4 = nist->FindOrBuildMaterial("G4_PbWO4");

    // Get from NIST the air material (used for the overworld)
    G4Material* world_mat = nist->FindOrBuildMaterial("G4_AIR");

	// Define variables for building complex materials
    G4double density;
    G4int ncomponents;
    G4int natoms;
	
	// Build manually the PbF2, since it does not exist in the NIST repository
	G4Element* F = nist->FindOrBuildElement(9);
	G4Element* Pb = nist->FindOrBuildElement(82);
	density = 7.770 *g/cm3;
	
	G4Material* PbF2 = new G4Material("PbF2", density, ncomponents= 2);
	
	PbF2->AddElement(Pb, natoms=1);
	PbF2->AddElement(F, natoms=2);

	//===================================================================================================
    //									DEFINITION OF THE WORLD
    //===================================================================================================
	// Define the boolean variable for the checks in the overlaps
	G4bool checkOverlaps = true;

    // Build the WORLD volume. For this volume and for the others, all volumes (solid, logical, physical)
    // should be built separately
    G4Box* solidWorld = new G4Box("World",10*m,10*m,20*m);
    G4LogicalVolume* logicWorld = new G4LogicalVolume(solidWorld, world_mat, "World");
    G4VPhysicalVolume* physWorld = new G4PVPlacement
                        (0,                    // no rotation
                        G4ThreeVector(),       // centre position
                        logicWorld,            // its logical volume
                        "World",               // its name
                        0,                     // its mother volume
                        false,                 // no boolean operation
                        0,                     // copy number
                        checkOverlaps);        // overlaps checking
						
						
	// Hide the overworld from a visual point of view (otherwise you can't see inside it in the rendering)
	G4VisAttributes* worldColor= new G4VisAttributes(G4Colour(1.,1.,1.));
	worldColor->SetVisibility(false);
	logicWorld->SetVisAttributes(worldColor);
	
	//===================================================================================================
    // 									DEFINITION OF THE CRYSTAL VOLUME
    //===================================================================================================
    // Define the target crystal, with length expressed in units of X0 and large lateral dimension.
    // But first, use "ifPbF2" to define whether the material will be PbF2 (-> true) or PbWO4 (-> false)
    // and define the length in X0 units
	G4bool ifPbF2 = true;
	G4double thick_scale = 30;
    // G4double latdimCrist = 2*cm; // Lat dimension good for graphics
    G4double latdimCrist = 40*cm;   // Lat dimension good for physics
	G4double thicknessCrist;        // I have to declare the variables in the global scope, then fill them
	G4Box* Crystal;
	G4LogicalVolume* logicCrystal;

    if (ifPbF2) {
    	// Assume the crystal is PbF2
    	thicknessCrist = thick_scale * PbF2->GetRadlen();
	    Crystal = new G4Box("Crystal", latdimCrist/2, latdimCrist/2, thicknessCrist/2);
	    logicCrystal = new G4LogicalVolume(Crystal,PbF2,"Crystal");
    }
    else {
    	// Assume the crystal is PbWO4
    	thicknessCrist = thick_scale * PbWO4->GetRadlen();
	    Crystal = new G4Box("Crystal", latdimCrist/2, latdimCrist/2, thicknessCrist/2);
	    logicCrystal = new G4LogicalVolume(Crystal,PbWO4,"Crystal");
    }
	
 
    new G4PVPlacement(0,
					G4ThreeVector(0, 0, 5*cm + thicknessCrist/2),
					logicCrystal,
					"Crystal",
					logicWorld,
					false,
					0,
					checkOverlaps);

	//===================================================================================================
    // 									PARAMETERS FOR THE GRAPHICS
    //===================================================================================================
	// Crystal target volume
	G4VisAttributes* absColor = new G4VisAttributes(G4Colour(1.,0.,0., 1.));
	absColor->SetVisibility(true);
	absColor->SetForceSolid(true);
	logicCrystal->SetVisAttributes(absColor);
	
	// Always return the physical World!
	return physWorld;
}

void DetectorConstruction::ConstructSDandField()
{

}