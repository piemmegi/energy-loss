# Energy loss simulation framework

Complete simulation of the energy loss of an input particle (either electron, positron or photon) incident on a block of amorphous material.

Author: [P. Monti-Guarnieri](mailto:pmontiguarnieri@studenti.uninsubria.it)

# A short guide on how to install and use Geant4 on WSL2

For more details please see the [Klever guide on the installation process](https://gitlab.com/piemmegi/klever2021_simulation.git)

# Quick commands and memo

- **Run for grahics**: start XLaunch -> Multiple windows -> Start no client -> Uncheck "Native opengl" and check "Disable access control"; then
```bash
./exampleB1
/control/execute run1.mac
```

- **Run for physics (single-run mode)**: edit `CERN2022/src/DetectorConstruction.cc` and `CERN2022/run1.mac` to set the crystal target (material and length), the statistics and the primary beam (energy and type). Then, source Geant4, make and run (launch everything from the `CERN2022-build` folder):
```bash
cd CERN2022-build
source ~/Geant4.11.0.2/geant4-11.0.2-install/bin/geant4.sh
cmake -DGeant4_DIR=~/Geant4.11.0.2/geant4-11.0.2-install/lib/Geant4-11.0.2/ ../CERN2022 && make -j4
./exampleB1 run1.mac
```

- **Run for physics (multi-run mode)**: edit `mastercall.sh` by setting the parameters for the runs to be executed. Two scans are performed, in both energy of the primary beam and length of the target material: define the intervals for the scans + other variables which will be kept fixed (target material, statistics per single run, primary beam type). Then, launch the `mastercall.sh` script from the `CERN2022-build` folder. All the outputs will be saved in the `Outputfiles` folder.
```bash
cd CERN2022-build
../mastercall.sh
```